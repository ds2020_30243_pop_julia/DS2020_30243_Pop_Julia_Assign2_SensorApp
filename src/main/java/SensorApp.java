import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

import business.ActivityProducer;
import entities.Activity;

public class SensorApp {

	@SuppressWarnings("unused")
	private static ActivityProducer activityProducer;
	@SuppressWarnings("unused")
	private static Activity activity;
	@SuppressWarnings("unused")
	private static BufferedReader reader;
	
	public static void main(String[] args) {
		activityProducer = new ActivityProducer();
		activity = new Activity();
		
		activityProducer.connect();
		
		Thread producerThread = new Thread() {
			public void run(){
		    	
				try {
					reader = new BufferedReader(new FileReader("activity.txt"));
					String line = reader.readLine();
					while (line != null) {
						String[] lineContent = line.split("		", 3);

						activity = new Activity();
						activity.setPatient_id(UUID.fromString("5c2494a3-1140-4c7a-991a-a1a2561c6bc2"));
						activity.setStart(lineContent[0]);
						activity.setEnd(lineContent[1]);
						activity.setActivity(lineContent[2].replaceAll("\t", ""));
						
						activityProducer.publishActivity(activity);
						Thread.sleep(1000);
						
						// read next line
						line = reader.readLine();
					}
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		};
		
		producerThread.start();
		
	}

}
