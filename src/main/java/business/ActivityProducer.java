package business;

import entities.Activity;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class ActivityProducer {
	
	private CachingConnectionFactory connectionFactory;

	public void connect() {
		connectionFactory = new CachingConnectionFactory("sparrow.rmq.cloudamqp.com");
        connectionFactory.setUsername("boyrjkey");
        connectionFactory.setPassword("Qyw89sPcD_hNivow241cOH11AT3-5kSe");
        connectionFactory.setVirtualHost("boyrjkey");

        //Recommended settings
        connectionFactory.setRequestedHeartBeat(30);
        connectionFactory.setConnectionTimeout(30000);

        //Set up queue, exchanges and bindings
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        Queue queue = new Queue("sensor_data");
        admin.declareQueue(queue);
        TopicExchange exchange = new TopicExchange("caregiver_topic");
        admin.declareExchange(exchange);
        admin.declareBinding(
                BindingBuilder.bind(queue).to(exchange).with("caregiver_topic.*"));

	}
	public void publishActivity(Activity activity){
	
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        Gson gson = new GsonBuilder().create();
        String message = new Gson().toJson(activity);

        System.out.println(" [x] Sent '" + message + "'");
        template.convertAndSend("caregiver_topic", "caregiver_topic.bar", message);
        

	}
		/*
		 ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost("localhost");
		    try (Connection connection = factory.newConnection();
		         Channel channel = connection.createChannel()) {
		        channel.exchangeDeclare("EXCHANGE_NAME", "fanout");
		        Gson gson = new GsonBuilder().create();
		        String message = new Gson().toJson(activity);
		        
		        channel.basicPublish("EXCHANGE_NAME", "", null, message.getBytes("UTF-8"));
		        System.out.println(" [x] Sent '" + message + "'");
		    } catch (TimeoutException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		*/
}
